# Online resource

This is public repository without access restriction.  
For internal knowledge and project implementation, please contact K.Jerdphan / Tanakorn for internal access.  

**New Entry:**  
Suitable for non coder who would like to explore the field  
[-Quick grab on Youtube](https://youtu.be/ukzFI9rgwfU)  
[-Tutorial Youtube by edureka](https://www.youtube.com/watch?v=b2q5OFtxm6A)  
[-Article: ML for Beginner1: Basic overall concept of ML](https://towardsdatascience.com/introduction-to-machine-learning-for-beginners-eed6024fdb08)  
[-Article: ML for Beginner2: Basic algorithm of ML](https://towardsdatascience.com/machine-learning-for-beginners-d247a9420dab)  

**Machine Learning Quick Course:**   
Suitable for New entry who would like to develop your skill further(You have got to code some in order to undderstand it deeper). For type (or major) of ML, please see New Entry session for more information  
[-Paid: Machine Learning with Python by datacamp](https://www.datacamp.com/tracks/machine-learning-with-python)  
[-Free: Udacity Free course (Part of ML Engineer Nanodegree, upgradable)](https://www.udacity.com/course/intro-to-machine-learning--ud120)  
[-Free: Online learning Resource by fast.ai](https://www.fast.ai/)
[-Free: Entry for deep learning(Part of deep learning specialization, upgradable)](https://www.coursera.org/learn/neural-networks-deep-learning?specialization=deep-learning)  
[-Paid: Data science: ML Supervised&Nonsupervised](https://www.udemy.com/machine-learning-data-science-deep-learning-neural-networks/)  

**Machine Learning Nanodegree**  
Choice depends on your preferences. For Deep Learning, I recommend coursera. Else, for wide knowledge, I recommend Udacity  
[-Paid: Nanodegree ML Engineer by Udacity](https://www.udacity.com/course/machine-learning-engineer-nanodegree--nd009t)  
[-Paid: ML for Trading](https://www.udacity.com/course/ai-for-trading--nd880)
[-Paid: Specialization focus at Deep Learning](https://www.coursera.org/specializations/deep-learning)  

**Required Mathematics:**  
[-Free: Calculus by Khan Academy](https://www.khanacademy.org/math/differential-calculus)  
[-Free: linear_Algebra by fast.ai](https://github.com/fastai/numerical-linear-algebra/blob/master/README.md)  

**Coding:**  
http://Codecademy.com  
http://codecombat.com  
https://docs.python.org/3/tutorial/index.html

**Graph Developer:**  
[-Free: Neo4j Graph Database (need SQL prerequisite)](https://neo4j.com/graphacademy/)

**SQL:**   
[-Free, paidable for cer.: SQL for data science](https://www.coursera.org/learn/sql-data-science)  

**For Management:**  
It is designed to educate AI capabilities.  
[-Free, paidable for cer.: AI for Everyone by Coursera](https://www.coursera.org/learn/ai-for-everyone)  
[-Paid: Executive Data Science Specialization by Coursera](https://www.coursera.org/specializations/executive-data-science)

Check command_git.txt for information on how to use git with vscode

For collaborative contributors, please feel free to add online course to this page.
Feel free to ask tanakorn.dha@uobam.co.th if you got any question.  